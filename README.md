# prettier-config-nekoziroo

[prettier](https://github.com/prettier/prettier) config for nekoziroo

## Installation

Install with npm:

```bash
npm install --save-dev prettier-config-nekoziroo
```

Install with yarn:

```bash
yarn add --dev prettier-config-nekoziroo
```

## Usage

```javascript
// prettier.config.js
module.exports = require('prettier-config-nekoziroo');
```
